package pemlan3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class KHSApplication {
    private static List<MataKuliah> mataKuliahList = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input data mahasiswa
        System.out.println("Masukkan Data Mahasiswa");
        System.out.print("NIM: ");
        String nim = scanner.nextLine();
        System.out.print("Nama: ");
        String nama = scanner.nextLine();
        Mahasiswa mahasiswa = new Mahasiswa(nim, nama);

        // Input data mata kuliah
        System.out.println("\nMasukkan Data Mata Kuliah (MK)");
        System.out.print("Kode MK: ");
        String kode = scanner.nextLine();
        System.out.print("Nama MK: ");
        String namaMK = scanner.nextLine();
        System.out.print("Nilai Angka: ");
        int nilaiAngka = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        MataKuliah mataKuliah = new MataKuliah(kode, namaMK, nilaiAngka);
        mataKuliahList.add(mataKuliah);

        // Cetak KHS
        System.out.println("\nKartu Hasil Studi (KHS)");
        System.out.println("Mahasiswa:");
        System.out.println("NIM: " + mahasiswa.getNim());
        System.out.println("Nama: " + mahasiswa.getNama());
        System.out.println("\nMata Kuliah (MK)");
        for (MataKuliah mk : mataKuliahList) {
            System.out.println("Kode MK: " + mk.getKode());
            System.out.println("Nama MK: " + mk.getNamaMK());
            System.out.println("Nilai Huruf: " + mk.getNilaiHuruf());
            System.out.println();
        }
    }
}
