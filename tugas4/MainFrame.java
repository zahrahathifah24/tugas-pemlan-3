package tugasGUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class MainFrame extends JFrame {
    private JTextField nameField;
    private JTextField emailField;
    private JTable table;
    private DefaultTableModel tableModel;
    private UserDAO userDAO;

    public MainFrame() {
        userDAO = new UserDAO();

        setTitle("MySQL Database App");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        // Panel Input
        JPanel inputPanel = new JPanel(new GridLayout(3, 2));
        inputPanel.add(new JLabel("Name:"));
        nameField = new JTextField();
        inputPanel.add(nameField);
        inputPanel.add(new JLabel("Email:"));
        emailField = new JTextField();
        inputPanel.add(emailField);

        JButton saveButton = new JButton("Save");
        inputPanel.add(saveButton);

        add(inputPanel, BorderLayout.NORTH);

        // Table
        tableModel = new DefaultTableModel(new Object[]{"ID", "Name", "Email"}, 0);
        table = new JTable(tableModel);
        add(new JScrollPane(table), BorderLayout.CENTER);

        // Load Data Button
        JButton loadButton = new JButton("Load Data");
        add(loadButton, BorderLayout.SOUTH);

        // Event Listeners
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadData();
            }
        });
    }

    private void saveData() {
        String name = nameField.getText();
        String email = emailField.getText();
        User user = new User(0, name, email); // id = 0 karena akan di-auto-increment oleh database
        userDAO.saveUser(user);
        JOptionPane.showMessageDialog(this, "Data saved successfully!");
        loadData(); // Refresh data after save
    }

    private void loadData() {
        List<User> users = userDAO.getAllUsers();
        tableModel.setRowCount(0); // Clear existing data

        for (User user : users) {
            tableModel.addRow(new Object[]{user.getId(), user.getName(), user.getEmail()});
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
}

